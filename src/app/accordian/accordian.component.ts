import { Component, OnInit } from '@angular/core';
import { MatExpansionModule, MatButtonModule } from '@angular/material';

@Component({
  selector: 'app-accordian',
  templateUrl: './accordian.component.html',
  styleUrls: ['./accordian.component.css']
})
export class AccordianComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  panelOpenState: boolean = false;

}
