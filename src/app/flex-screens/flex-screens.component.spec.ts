import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlexScreensComponent } from './flex-screens.component';

describe('FlexScreensComponent', () => {
  let component: FlexScreensComponent;
  let fixture: ComponentFixture<FlexScreensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlexScreensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlexScreensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
