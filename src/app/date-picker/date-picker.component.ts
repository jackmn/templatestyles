import { Component, OnInit, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { $ } from 'protractor';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DatePickerComponent implements OnInit {
  @ViewChild('dp', { static: false }) dp: ElementRef;
  constructor() { }

  doneIsTrue: boolean = false;
  ngOnInit() {
  }

  // function() {
  //   $('#datepicker').datepicker({
  //     startDate: new Date(),
  //     multidate: true,
  //     format: "dd/mm/yyyy",
  //     daysOfWeekHighlighted: "5,6",
  //     datesDisabled: ['31/08/2017'],
  //     language: 'en'
  //   }).on('changeDate', function(e) {
  //     // `e` here contains the extra attributes
  //     (this).find('.input-group-addon .count').text(' ' + e.dates.length);
  //   });
  // };

  /*
   function() {
      $('#datepicker').datepicker({
        startDate: new Date(),
        multidate: true,
        format: "dd/mm/yyyy",
        daysOfWeekHighlighted: "5,6",
        datesDisabled: ['31/08/2017'],
        language: 'en'
      }).on('changeDate', function(e) {
        // `e` here contains the extra attributes
        $(this).find('.input-group-addon .count').text(' ' + e.dates.length);
      });
    };
  
  */

  // dateFn(){
  //   console.log(this.dp);
  //   console.log('hiii',this.dp.nativeElement.querySelector(".input-group-addon"));

  //   this.dp.nativeElement.querySelector(".input-group-addon")

  // }

  daysSelected: any[] = [];
  event: any;

  isSelected = (event: any) => {
    const date =
      event.getFullYear() +
      "-" +
      ("00" + (event.getMonth() + 1)).slice(-2) +
      "-" +
      ("00" + event.getDate()).slice(-2);
    return this.daysSelected.find(x => x == date) ? "selected" : null;
  };


  select(event: any, calendar: any) {
    const date =
      event.getFullYear() +
      "-" +
      ("00" + (event.getMonth() + 1)).slice(-2) +
      "-" +
      ("00" + event.getDate()).slice(-2);
    const index = this.daysSelected.findIndex(x => x == date);
    if (index < 0) this.daysSelected.push(date);
    else this.daysSelected.splice(index, 1);

    calendar.updateTodaysDate();
    console.log('checking', this.daysSelected)
  }
}