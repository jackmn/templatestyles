import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToButton(){
    this.router.navigate(["button"])
  }

  goToText(){
    this.router.navigate(["text"])
  }

  goToDropdown() {
    this.router.navigate(["dropdown"])

  }
  goToCard() {
    this.router.navigate(["card"])

  }

  goToAccordion() {
    this.router.navigate(["accordian"])

  }

  goToIcons() {
    this.router.navigate(["icons"])
  }

  goToDatePicker(){
    this.router.navigate(["datePicker"])
  }

  goToSearch(){
    this.router.navigate(["searchBars"])
  }
  goToCheckBox() {
    this.router.navigate(["checkBoxes"])

  }
  goToModal() {
    this.router.navigate(["modal"])

  }

  goToModals() {
    this.router.navigate(["modal"])
  }
}
