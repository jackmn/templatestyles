import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPageComponent } from './main-page/main-page.component';
import { TextComponent } from './text/text.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { IconsComponent } from './icons/icons.component';
import { AccordianComponent } from './accordian/accordian.component';
import { ColorComponent } from './color/color.component';
import { SearchBarsComponent } from './search-bars/search-bars.component';
import { TabsComponent } from './tabs/tabs.component';
import { CheckBoxesComponent } from './check-boxes/check-boxes.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ModalComponent } from './modal/modal.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { ChatPopUpComponent } from './chat-pop-up/chat-pop-up.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';
import { CardComponent } from './card/card.component';
import { ChartsComponent } from './charts/charts.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { makeBindingParser } from '@angular/compiler';
import { AppComponent } from './app.component';
import { FilteringComponent } from './filtering/filtering.component';
import { FlexScreensComponent } from './flex-screens/flex-screens.component';


const routes: Routes = [
  {
    path: "", component: ButtonsComponent
  },
  {
    path:"button", component: ButtonsComponent
  },
  {
    path: "text", component: TextComponent
  },
  {
    path: "dropdown", component: DropdownComponent
  },
  {
    path: "icons", component: IconsComponent
  },
  {
    path: "accordian", component: AccordianComponent
  },
  {
    path: "color", component: ColorComponent
  },
  {
    path: "searchBars", component: SearchBarsComponent
  },
  {
    path: "tabs", component: TabsComponent
  },
  {
    path: "checkBoxes", component: CheckBoxesComponent
  },
  {
    path: "sidebar", component: SidebarComponent
  },
  {
    path: "navbar", component: NavbarComponent
  },
  {
    path: "modal", component: ModalComponent
  },
  {
    path: "datePicker", component: DatePickerComponent
  },
  {
    path: "chatPopUp", component: ChatPopUpComponent
  },
  {
    path: "lifecycle", component: LifecycleComponent
  },
  {
    path: "card", component: CardComponent
  },
  {
    path: "charts", component: ChartsComponent
  },
  {
    path: "filtering", component: FilteringComponent
  }, 
  {
    path: "flex", component: FlexScreensComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


