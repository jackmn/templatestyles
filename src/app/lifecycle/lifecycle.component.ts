import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-lifecycle',
  templateUrl: './lifecycle.component.html',
  styleUrls: ['./lifecycle.component.css']
})
export class LifecycleComponent implements OnInit {

  @Input('lifeCyclePosition') lifeCyclePosition;

  constructor() { }

  ngOnInit() {
  }

  changeColour(event) {
    let allDivs = event.path[1].children;
    // console.log(allDivs);
    let before = true;

    for (let i = 0; i < allDivs.length; i++) {
      const element = allDivs[i];
      element.classList.remove('blue', 'red');

      if(element !== event.target){
        if(before){
          element.classList.add('red');
        }
      }
      if(element !== event.target && !before){
        element.classList.add('blue');
      }
      else{
        element.classList.add('red');
        before = false;
      }
    }

    // if(event.target.classList.contains('blue')){
    //   event.target.classList.remove('blue');
    //   event.target.classList.add('red');
    // }
    // else if(event.target.classList.contains('red')){
    //   event.target.classList.remove('red');
    //   event.target.classList.add('blue');
    // }
  }

}
