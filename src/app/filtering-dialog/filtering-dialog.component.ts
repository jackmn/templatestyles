import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { FilteringComponent } from '../filtering/filtering.component';
import { element } from 'protractor';
import { Routes, RouterModule, Route, Router } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { FilteringService } from '../filtering/filtering.service';


@Component({
    selector: 'app-filtering-dialog',
    templateUrl: './filtering-dialog.component.html',
    styleUrls: ['./filtering-dialog.component.css']
})


export class FilteringDialogComponent implements OnInit {

    titles = [
        "Entity",
        "Inspector",
        "Inspection Area",
        "Inspection Status",
        "Inspection Agency",
        
    ];

    searchedTerm: string;
    savedFilter: any;
    showSaveFilterSection: boolean;
    filterName: string;

    allFilterObjects: any = {entity:[], inspector:[], agency:[], status:[], area:[]};
    allSelectedFilters: any = {entity:[], inspector:[], agency:[], status:[], area:[]};

    constructor(
        public dialogRef: MatDialogRef<FilteringDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private filterService: FilteringService) { }


    ngOnInit() {
        this.searchedTerm = '';
        
        this.allFilterObjects = this.filterService.prepareFilterObjectsForUsing_0021(this.data.fullFilterList, this.data.appliedFilterList);
        this.showSaveFilterSection = false;
    }

    closeDialogOnCancel_0014(dialogRef: MatDialogRef<FilteringDialogComponent>): void {
        dialogRef.close();
    }

    settingClickedStatus_0015(list: any, type: any, status: boolean): void {
        for (let index = 0; index < list.length; index++) {
            const element = list[index];
            if (element.name === type) {
                
                element.clicked = status;
            }
        }
    }

    toggleFilterTypeList_0016(type: any, name: string, list: any): void {

        let id = type + name;
        let status = (<HTMLInputElement>document.getElementById(id)).checked;

        this.settingClickedStatus_0015(list, type, status);
    }

    // clearFilters(): void {
    //     for (let index = 0; index < this.displayEntityList.length; index++) {
    //         const element = this.displayEntityList[index];
    //         element.clicked = false;
    //     }
    //     for (let index = 0; index < this.displayInspectorList.length; index++) {
    //         const element = this.displayInspectorList[index];
    //         element.clicked = false;
    //     }
    // }

    exportFilterDialogObject_0017(): void {
        this.allSelectedFilters = this.filterService.prepareFilterObjectsForExport_022(this.allFilterObjects);
        this.dialogRef.close({data: this.allSelectedFilters});
    }

    saveFilter_0013(): void {
        this.allSelectedFilters = this.filterService.prepareFilterObjectsForExport_022(this.allFilterObjects);
        this.savedFilter = this.allSelectedFilters;
        this.savedFilter.name = this.filterName;
        this.dialogRef.close({data: this.allSelectedFilters});
    }

    toggleBoolean(myBool){
        myBool = !myBool;
        return myBool;
    }

}
