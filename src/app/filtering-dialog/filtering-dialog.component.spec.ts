import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PipeTransform, Pipe } from '@angular/core';
import { FilteringDialogComponent } from './filtering-dialog.component';
import { DebugElement } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { dialogFilterPipe } from './dialog-filter.pipe';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FilteringService } from '../filtering/filtering.service';
import { dummyVals, dummyFilters } from '../filtering-dialog/dummy-filtering-dialog'


xdescribe('FilteringDialogComponent', () => {
  let component: FilteringDialogComponent;
  let fixture: ComponentFixture<FilteringDialogComponent>;
  let el: DebugElement;
  let filterService: any;

  beforeEach(async(() => {

    const filterServiceSpy = jasmine.createSpyObj('FilteringService', ['prepareFilterObjectsForExport_0022', 'prepareFilterObjectsForUsing_0021'])
    TestBed.configureTestingModule({
      declarations: [ FilteringDialogComponent, dialogFilterPipe ],
      imports: [ FormsModule, NoopAnimationsModule ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: FilteringService,
          useValue: filterServiceSpy
        },

      ]
    })
    .compileComponents()
    .then( () => {
      fixture = TestBed.createComponent(FilteringDialogComponent);
      component = fixture.componentInstance;
      el = fixture.debugElement;
      filterService = TestBed.get(FilteringService);
    })
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilteringDialogComponent);
    component = fixture.componentInstance;
    component.showSaveFilterSection = false;

    filterService.prepareFilterObjectsForUsing_0021.and.returnValue(dummyFilters);

    fixture.detectChanges();
  });

  it('should create', () => {
    // const pipeSpy = jasmine.createSpyObj('dialogFilterPipe', ['transform'])
    expect(component).toBeTruthy();
  });

  it('should change from false to true when called', () => {
    expect(component.toggleBoolean(component.showSaveFilterSection)).toBeTruthy();
  });

});
