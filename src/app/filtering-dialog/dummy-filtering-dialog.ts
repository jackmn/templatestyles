export const dummyVals: any = [
    { id: 1, entity: "Dublin", inspector: "Dawhee McMorrow", agency: "FDA", status: "Complete", area: "Packing" },
    { id: 2, entity: "London", inspector: "Gak Mac", agency: "FDA", status: "Complete", area: "Lab Systems" },
    { id: 3, entity: "Paris", inspector: "Crendan O'Bonnell", agency: "HPRA", status: "Complete", area: "Lab Systems" },
    { id: 4, entity: "Dublin", inspector: "Matty Runsquick", agency: "HPRA", status: "In Progress", area: "Packing" },
    { id: 5, entity: "London", inspector: "Dawhee McMorrow", agency: "FDA", status: "In Progress", area: "Accounting" },
    { id: 6, entity: "Paris", inspector: "Gak Mac", agency: "WHO", status: "In Progress", area: "Lab Systems" },
    { id: 7, entity: "Dublin", inspector: "Crendan O'Bonnell", agency: "FDA", status: "In Progress", area: "Packing" },
    { id: 8, entity: "London", inspector: "Matty Runsquick", agency: "FDA", status: "In Progress", area: "Packing" },
];

export const dummyFilters: any = {
    entity: [],
    inspector: [],
    agency: [],
    status: [],
    area: [],
}