import { PipeTransform, Pipe } from '@angular/core';


@Pipe({
    name: 'filterSearch'
})
export class dialogFilterPipe implements PipeTransform {
    transform(listOfFilters: any[], searchedTerm: string) {
        for (let index = 0; index < listOfFilters.length; index++) {
            const element = listOfFilters[index];
            if (!element.name || !searchedTerm || searchedTerm == '') {
                return listOfFilters
            }
        }
        return listOfFilters.filter(item =>
            (item.name.toLowerCase().indexOf(searchedTerm.toLowerCase()) !== -1)
            || (item.clicked)
        );
    }
}