import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent {
  single: any[];
  view: any[] = [700, 400];

  // options
  gradient: boolean = true;
  showLegend: boolean = true;
  showLabels: boolean = true;
  isDoughnut: boolean = false;
  legendPosition: string = 'right';
  animations: [];
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  xAxisLabel = 'X axis label';
  showYAxisLabel = true;
  yAxisLabel = 'Y axis Label';

  colorScheme = {
    domain: ['#1F78B4', '#B2DF8A', '#A6CEE3',]
  };

  constructor() {
    this.single = [
  {
    "name": "Label1",
    "value": 50
  },
  {
    "name": "Label2",
    "value": 20
  },
  {
    "name": "Label3",
    "value": 30
  },
];
}
}