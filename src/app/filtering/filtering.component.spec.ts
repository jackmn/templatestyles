import { async, ComponentFixture, TestBed, fakeAsync, flush } from '@angular/core/testing';

import { FilteringComponent } from './filtering.component';
import {  MatDialogModule, MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DebugElement, ViewEncapsulation } from '@angular/core';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FilteringDialogComponent } from '../filtering-dialog/filtering-dialog.component';


fdescribe('FilteringComponent', () => {
  let component: FilteringComponent;
  let fixture: ComponentFixture<FilteringComponent>;
  let el: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilteringComponent ],
      imports: [ NoopAnimationsModule ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        },
        {
          provide: MatDialogModule, 
          useValue: {}
        },
        {provide: MatDialog, 
         useValue: {}
      }]
      
    })
    .compileComponents()
    .then( () => {
      fixture = TestBed.createComponent(FilteringComponent);
      component = fixture.componentInstance;
      el = fixture.debugElement
    })
  }));

  // beforeEach(() => {
  //   fixture = TestBed.createComponent(FilteringComponent);
  //   component = fixture.componentInstance;
  //   fixture.detectChanges();
    
  // });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open dialog', fakeAsync(() => {
    // let dialogComponent: FilteringDialogComponent;
    // console.log("David");
    
    const result = component.openFilterModal_0023();
    
    // console.log("David 2");
    // // component.openFilterModal_0023()
    // const button = el.queryAll(By.css("filter-modal-btn-opn"));

    // let dialogSpy = jasmine.createSpyObj('dialog', ['open']);
    // console.log(button);
    
    fixture.detectChanges();

    flush();

    const dialog = el.queryAll(By.css(".mat-dialog-container")) 

    expect(dialog).toBeTruthy("Dialog wasn't truthy");

  }))
});
