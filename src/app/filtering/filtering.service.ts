import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class FilteringService {

    testList: Array<any>;

    constructor() {
        this.testList = [
            { id: 1, entity: "Dublin", inspector: "Dawhee McMorrow", agency: "FDA", status: "Complete", area: "Packing" },
            { id: 2, entity: "London", inspector: "Gak Mac", agency: "FDA", status: "Complete", area: "Lab Systems" },
            { id: 3, entity: "Paris", inspector: "Crendan O'Bonnell", agency: "HPRA", status: "Complete", area: "Lab Systems" },
            { id: 4, entity: "Dublin", inspector: "Matty Runsquick", agency: "HPRA", status: "In Progress", area: "Packing" },
            { id: 5, entity: "London", inspector: "Dawhee McMorrow", agency: "FDA", status: "In Progress", area: "Accounting" },
            { id: 6, entity: "Paris", inspector: "Gak Mac", agency: "WHO", status: "In Progress", area: "Lab Systems" },
            { id: 7, entity: "Dublin", inspector: "Crendan O'Bonnell", agency: "FDA", status: "In Progress", area: "Packing" },
            { id: 8, entity: "London", inspector: "Matty Runsquick", agency: "FDA", status: "In Progress", area: "Packing" },
        ]; 
    }

    getInspectionList_0018(): any[] {
        return this.testList;
    }

    filterListLogic_0019(list, allFilters): any {
        let filteredList = list.filter(
            item => (allFilters.entity.includes(item.entity) || allFilters.entity.length == 0)
                && (allFilters.inspector.includes(item.inspector) || allFilters.inspector.length == 0)
                && (allFilters.status.includes(item.status) || allFilters.status.length == 0)
                && (allFilters.area.includes(item.area) || allFilters.area.length == 0)
                && (allFilters.agency.includes(item.agency) || allFilters.agency.length == 0));
        return filteredList;
    }

    getFilterOptions_0020(fullList): any {
        
        let filterLists = { entity: new Set(), inspector: new Set(), agency: new Set(), status: new Set(), area: new Set() };

        fullList.forEach(item => {
            filterLists.entity.add(item.entity);
            filterLists.inspector.add(item.inspector);
            filterLists.agency.add(item.agency);
            filterLists.status.add(item.status);
            filterLists.area.add(item.area);
        });
        return filterLists;
    }

    prepareFilterObjectsForUsing_0021(allFilterValues, appliedFilterValues): any {

        let filterObjects = {entity:[], inspector:[], agency:[], status:[], area:[]};

        let keys = Object.keys(allFilterValues);

        for(let key of keys){
            allFilterValues[key].forEach(item => {
                filterObjects[key].push({name: item, clicked: appliedFilterValues[key].includes(item)});
            });
        }
        return filterObjects;
    }

    prepareFilterObjectsForExport_022(allFilterValues): any{

        let filterObjects = {entity:[], inspector:[], agency:[], status:[], area:[]};

        let keys = Object.keys(filterObjects);

        for(let key of keys){
            allFilterValues[key].forEach(item => {
                if(item.clicked){
                    filterObjects[key].push(item.name);
                }
            });
        }
        return filterObjects;
    }
}
