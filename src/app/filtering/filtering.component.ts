import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FilteringDialogComponent } from '../filtering-dialog/filtering-dialog.component';
import { FilteringService } from './filtering.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';



@Component({
    selector: 'app-filtering',
    templateUrl: './filtering.component.html',
    styleUrls: ['./filtering.component.css'],
    encapsulation: ViewEncapsulation.None,

})
export class FilteringComponent implements OnInit {

    inspectionsList: any;
    dispInspectionsList: any;
    allFilterOptions: any;
    filtersFromModal: any;
    appliedFilterList: any;

    constructor(public dialog: MatDialog,
        private filterservice: FilteringService) { }

    ngOnInit() {
        this.inspectionsList = this.filterservice.getInspectionList_0018();
        this.dispInspectionsList = [...this.inspectionsList];
        this.allFilterOptions = this.filterservice.getFilterOptions_0020(this.inspectionsList);
        // console.log("ALL FILTER OPTIONS", this.allFilterOptions);

        this.appliedFilterList = {entity:[], inspector:[], agency:[], status:[], area:[]};
    }

    openFilterModal_0023() {
        const dialogRef = this.dialog.open(FilteringDialogComponent, {
            width: '80%',
            height: '80%',
            data: { 'fullFilterList': this.allFilterOptions, 'appliedFilterList': this.appliedFilterList },
        });

        dialogRef.afterClosed().subscribe(result => {
            if(result){
                this.filtersFromModal = result.data;
                this.appliedFilterList = {...this.filtersFromModal};
                this.applyFilterToSchedule_0024();
            }
        });
        return true;
    }

    applyFilterToSchedule_0024() {
        let entityLength = this.filtersFromModal.entity.length == 0;
        let inspectorLength = this.filtersFromModal.inspector.length == 0;
        let statusLength = this.filtersFromModal.status.length == 0;
        let areaLength = this.filtersFromModal.area.length == 0;
        let agencyLength = this.filtersFromModal.agency.length == 0;

        if (entityLength && inspectorLength && statusLength && areaLength && agencyLength) {
            this.dispInspectionsList = [...this.inspectionsList];
        }
        else {
            this.dispInspectionsList = this.filterservice.filterListLogic_0019(this.inspectionsList, this.filtersFromModal);
        }
    }

}

