import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  constructor() { }

  people = ["David", "Daniel", "Chris"];
  optionPicked : any;

  ngOnInit() {
    this.optionPicked = undefined;
  }

  chosenOption(name){
    console.log(name);
    this.optionPicked = name
  }

  validation(){  
    if(this.optionPicked == undefined || this.optionPicked == 0){
      return 0;
    } else if(this.optionPicked == "David"){
      return 1;
    }
    else{
      return -1
    }
  }
}
