import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  @Output() sendSidebarStatus: EventEmitter<Boolean> = new EventEmitter();

  constructor() { }

  expandedSideBarBoolean : Boolean;

  options = ["Close", "Information", "Calendar", "Report", "Handshake"];
  optionChosen : any;

  ngOnInit() {
    this.expandedSideBarBoolean = false;
    this.sendSidebarStatus.emit(this.expandedSideBarBoolean)
  }

  expandedView(){
    this.expandedSideBarBoolean = !this.expandedSideBarBoolean;
    this.sendSidebarStatus.emit(this.expandedSideBarBoolean)
  }

  optionPicked(option : any){
    console.log("Option", option);
    this.optionChosen = option;
    
  }

}
