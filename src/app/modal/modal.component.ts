import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog} from '@angular/material';
import { MyDialogComponent } from '../my-dialog/my-dialog.component';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  result :any;

  constructor(public dialog: MatDialog,
) {}

  dialogResult: any;
  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MyDialogComponent, {
      width: '1000px',
      data: 'hey whats up with you'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.result = result;
    });
  }

  
}


