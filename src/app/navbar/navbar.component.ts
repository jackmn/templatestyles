import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  navbarOptions : any[] = [
    {
      link : "button",
      name : "Buttons"
    },
    {
      link : "text",
      name : "Text"
    },
    {
      link : "dropdown",
      name : "Dropdown"
    },
    {
      link : "checkBoxes",
      name : "Check Box"
    },
    {
      link : "modal",
      name : "Modal"
    },
    {
      link : "card",
      name : "Card"
    },
    {
      link : "accordian",
      name : "Accordion"
    },
    {
      link : "icons",
      name : "Icons"
    },
    {
      link : "datePicker",
      name : "Date Picker"
    },
    {
      link : "searchBars",
      name : "Search"
    },
    {
      link : "charts",
      name : "Charts"
    },
    {
      link : "tabs",
      name : "Tabs"
    },
    {
      link : "lifecycle",
      name : "lifecycle"
    },
    {
      link : "filtering",
      name : "Filtering" 
    },
    {link: "flex",
    name: "flex"
    }
  ]

}
