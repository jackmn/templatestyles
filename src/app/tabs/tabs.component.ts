import { Component, OnInit } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import * as moment from 'moment'

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

    constructor() { }

    ngOnInit() {
        this.getDayOfMonthAppended_0007('2019-10-14 11:22:49.537')
    }


    formatDate_0001(date) {
        const presentationDate = moment(date).format('DDMMMYYYY').toUpperCase();
        return presentationDate;
    }

    getMonthName_0002(date) {
        const monthName = moment(date).format('MMM').toUpperCase();
        return monthName;
    }

    getDatesOfWeekends_0004(year) {

        console.time("throughYear");

        let yearlyWeekends = [];
        let nextYear = year + 1;
        let beginYear = moment(year + '-01-01');
        let endYear = moment(nextYear + '-01-01');

        for (let m = moment(beginYear); m.isBefore(endYear); m.add(1, 'days')) {
            let formattedMonth = m.format('DDMMMYYYY');
            if (this.isWeekend_0003(formattedMonth)) {
                yearlyWeekends.push(formattedMonth.toUpperCase());
            }
        }
        console.timeEnd("throughYear");

        return yearlyWeekends;
    }

    isWeekend_0003(date) {

        console.time("isWeekend");

        let isWeekend: boolean;
        let day: string;

        day = moment(date).format('dddd');

        if (day == 'Saturday' || day == 'Sunday') {
            isWeekend = true;
        } else {
            isWeekend = false;
        }

        console.timeEnd("isWeekend");

        return isWeekend;
    }

    getCurrentDate_0005() {
        let currentDate = moment().format('DDMMMYYYY').toUpperCase();
        return currentDate;
    }

    getDayOfMonth_0006(date) {
        let dayOfMonth = moment(date).format('D');
        return dayOfMonth;
    }

    getDayOfMonthAppended_0007(date) {
        let dayOfMonthAppended = moment(date).format('Do');
        return dayOfMonthAppended;
    }

    getDayOfWeek_0008(date) {
        let dayOfWeek = moment(date).format('dddd');
        return dayOfWeek;
    }

    getCurrentYear_0009() {
        let Currentyear = moment().format('YYYY');
        return Currentyear;
    }

    getCurrentMonth_0010() {
        let currentMonth = moment().format('MMM').toUpperCase();
        return currentMonth;
    }

    getYear_0011(date) {
        let year = moment(date).format('YYYY');
        return year;
    }
}
