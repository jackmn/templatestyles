import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatTabsModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { TabsComponent } from './tabs.component';
import * as moment from 'moment';
import { ngModuleJitUrl } from '@angular/compiler';

fdescribe('TabsComponent', () => {
  let component: TabsComponent;
  let fixture: ComponentFixture<TabsComponent>;
  let currentDate: string;
  let testDate: string;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsComponent ],
      imports: [ MatTabsModule, NoopAnimationsModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    currentDate = moment().format();
    testDate = "2004-05-25 11:22:49.537";

    fixture = TestBed.createComponent(TabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a month', () =>{
    const result = component.getMonthName_0002("2019-10-14 11:22:49.537");
    expect(result).toBe("OCT");
  });

  it('should have a formatted date', () =>{
    const result = component.formatDate_0001("2019-10-14 11:22:49.537");
    expect(result).toBe("14OCT2019");
  });

  it('it should check if a date is on the weekend', () => {
    const result = component.isWeekend_0003("2019-12-15");
    expect(result).toBe(true);
  });

  it('it should return all of the weekends in a year', () => {
    const result = component.getDatesOfWeekends_0004(2019);
    expect(result.length).toBe(104);
  });

  it('should return the current date', () => {
    let expectedResult = moment().format('DDMMMYYYY').toUpperCase()
    let result = component.getCurrentDate_0005();

    expect(result.length).toBe(9, "length was incorrect");
    expect(result).toBe(expectedResult, "date was incorrect");
  });

  it('should return the day of the month', () => {
    let month = moment(testDate).format('D')
    let result = component.getDayOfMonth_0006(testDate);

    // expect(result.length).toBe(2, "length was incorrect");
    expect(result).toBe(month, "month day was incorrect");
  });
  it('should return the appended day of the month', () => {
    let month = moment(testDate).format('Do')
    let result = component.getDayOfMonthAppended_0007(testDate);

    expect(result.length).toBe(4, "length was incorrect");
    expect(result).toBe(month, "appended month day was incorrect");
  });

  it('should return the day of the week', () => {
    let result = component.getDayOfWeek_0008(testDate);
    let expectedResult = moment(testDate).format('dddd');
    expect(result).toBe(expectedResult, "day of week was incorrect");
  });

  it('should get the current dates month', () => {
    let currentMonth = moment().format('MMM').toUpperCase()
    let result = component.getCurrentMonth_0010()
    expect(result).toEqual(currentMonth)
  })

  it('should get the current dates year', () => {
    let currentYear =  moment().format('YYYY')
    let result = component.getCurrentYear_0009()
    expect(result).toEqual(currentYear,"the current year is incorrect")
  })

  it('should get the year from an inputted date', () => {
    let result = component.getYear_0011('2019-10-14 11:07:08.767')
    expect(result).toEqual('2019', 'the year was not correct')
  })
 
  // it('should get the month as an integer from an input', () => {
  //   let result = component.getMonthNumeric_XXXX('2019-10-14 11:07:08.767')
  //   expect(result).toEqual('10')
  // })

});

