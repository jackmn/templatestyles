import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-check-boxes',
  templateUrl: './check-boxes.component.html',
  styleUrls: ['./check-boxes.component.css']
})
export class CheckBoxesComponent implements OnInit {

dummyData = [

{ id: 1, name: 'sample item checking to see how large items can grow' },
{ id: 2, name: 'order 2' },
{ id: 3, name: 'order 3' },
{ id: 4, name: 'order 4' }
];

  constructor() { }

  ngOnInit() {
  }


  optionClicked(index){
    console.log("box clicked -", index);
  }

  deleteClicked(index){
    console.log("delete clicked -", index);
  }

  addPressed(){
    console.log("add clicked");
  }

  approvePressed(){
    console.log("approve clicked");
  }
}
