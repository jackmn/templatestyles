import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { TextComponent } from './text/text.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { IconsComponent } from './icons/icons.component';
import { AccordianComponent } from './accordian/accordian.component';
import { ColorComponent } from './color/color.component';
import { SearchBarsComponent } from './search-bars/search-bars.component';
import { TabsComponent } from './tabs/tabs.component';
import { CheckBoxesComponent } from './check-boxes/check-boxes.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ModalComponent } from './modal/modal.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { ChatPopUpComponent } from './chat-pop-up/chat-pop-up.component';
import { LifecycleComponent } from './lifecycle/lifecycle.component';
import { CardComponent } from './card/card.component';
import { ChartsComponent } from './charts/charts.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule } from '@angular/forms';
import { MatExpansionModule, MatButtonModule, MatFormFieldModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule,  } from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/'
import {MatCardModule} from '@angular/material/card';

import {MatDialogModule} from '@angular/material';
import { MyDialogComponent } from './my-dialog/my-dialog.component';
import { FilteringComponent } from './filtering/filtering.component';
import { FilteringDialogComponent } from './filtering-dialog/filtering-dialog.component';
import { dialogFilterPipe } from './filtering-dialog/dialog-filter.pipe';

import { FlexScreensComponent } from './flex-screens/flex-screens.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    ButtonsComponent,
    TextComponent,
    DropdownComponent,
    IconsComponent,
    AccordianComponent,
    ColorComponent,
    SearchBarsComponent,
    TabsComponent,
    CheckBoxesComponent,
    SidebarComponent,
    NavbarComponent,
    ModalComponent,
    DatePickerComponent,
    ChatPopUpComponent,
    LifecycleComponent,
    CardComponent,
    ChartsComponent,
    MyDialogComponent,
    FilteringComponent,
    FilteringDialogComponent,
    dialogFilterPipe,
    FlexScreensComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    MatTabsModule,
    FormsModule,
    MatExpansionModule,
    MatButtonModule,
    NoopAnimationsModule, 
    MatMenuModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatDialogModule,
    MatFormFieldModule,
    
  ],
  entryComponents: [MyDialogComponent, FilteringDialogComponent ],
  providers: [MatDatepickerModule],
  bootstrap: [AppComponent],
  exports: [FilteringDialogComponent]
})
export class AppModule { }
